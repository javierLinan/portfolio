module.exports = function ( grunt ) {
    var config = {
        pkg: grunt.file.readJSON('package.json'),
        env: process.env,
        dir: {
            build: 'build',
            test: 'tests',
            compile: 'bin',
            doc: 'doc',
            templates: 'templates',
            src: 'src',
        },
        app_files: {
            entry_point: 'src/app/main.js',
            js  : [ 'src/**/*.js', '!src/assets/**/*.js' ],
            tpl : [ 'src/**/*.tpl.html' ],
            html: [ 'src/index.html' ],
            less: 'src/less/main.less',
            spec: [ 'tests/src/**/*.js' ],
            assets: [ 'src/assets/**/*' ],
        },
        vendor_files: {
            js: [
                'vendor/backbone/backbone.js',
                'vendor/backbone.localstorage/backbone.localStorage.js',
                'vendor/jquery/dist/jquery.js',
                'vendor/lodash/dist/lodash.underscore.js',
                // 'vendor/requirejs/require.js',
                'vendor/requirejs-text/text.js',
                'vendor/handlebars/handlebars.amd.js',
                'vendor/bootstrap/dist/js/bootstrap.js',
                'vendor/canvasloader/js/heartcode-canvasloader.js',
                'vendor/almond/almond.js',
            ],
            css: [],
            assets: []
        }
    };


    require('load-grunt-tasks')(grunt);

    grunt.util._.extend(config, loadConfig('./tasks/options/'));

    grunt.initConfig(config);

    grunt.loadTasks('tasks');

    grunt.renameTask( 'watch', 'delta' );

    grunt.registerTask('watch', ['build', 'delta']);

    function loadConfig(path) {
        var glob = require('glob'),
        object = {},
        key;

        glob.sync('*', {cwd: path}).forEach(function(option) {
            key = option.replace(/\.js$/,'');
            object[key] = require(path + option);
        });

        return object;
    }
};
