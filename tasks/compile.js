module.exports = function(grunt) {
    grunt.registerTask( 'compile', [
        'clean:compile', 'jshint', 'less:compile', 'concat:compile_css', 'copy:compile_app_assets', 'requirejs:compile', 'index:compile'
    ]);
}
