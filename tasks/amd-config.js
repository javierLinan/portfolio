module.exports = function(grunt) {

    grunt.registerMultiTask( 'amd-config', 'Process the requirejs config template', function () {
        var dir = this.data.dir,
            entryPoint = this.data.entry_point || '';

        var jsFiles = this.filesSrc.map(function(file) {
            return file.replace(dir + '/', '');
        });

        grunt.file.copy(grunt.config('dir.templates') + '/' + this.data.tpl, dir + '/config-amd.js', {
            process: function ( contents, path ) {
                return grunt.template.process( contents, {
                    data: {
                        src: jsFiles,
                        entry_point: entryPoint,
                    }
                });
            }
        });
    });
};
