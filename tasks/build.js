module.exports = function(grunt) {
    grunt.registerTask( 'build', [
        'clean:build', 'jshint', 'less:build', 'concat:build_css', 'copy:build_app_assets', 'copy:build_app_js', 'copy:build_app_tpl',
        'copy:build_vendor_js', 'index:build', 'amd-config:build'
    ]);
}
