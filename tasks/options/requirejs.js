module.exports = {
    compile: {
        options: {
            baseUrl: '<%= dir.build %>',
            mainConfigFile: '<%= dir.build %>/config-amd.js',
            name: 'vendor/almond/almond',
            include: ['src/app/main'],
            optimize: 'uglify2',
            wrap: true,
            preserveLicenseComments: false,
            out: '<%= dir.compile %>/assets/<%= pkg.name %>-<%= pkg.version %>.js',
        }
    }
};
