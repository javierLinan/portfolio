module.exports = {

    options: {
        livereload: true
    },

    gruntfile: {
        files: 'Gruntfile.js',
        tasks: [ 'jshint:gruntfile' ],
        options: {
            livereload: false
        }
    },

    assets: {
        files: [ '<%= app_files.assets %>' ],
        tasks: [ 'copy:build_app_assets', 'copy:build_vendor_assets' ]
    },

    jssrc: {
        files: [ '<%= app_files.js %>' ],
        tasks: [ 'jshint:src', 'copy:build_app_js' ]
    },

    html: {
        files: [ 'templates/index-build.tpl.html' ],
        tasks: [ 'index:build' ]
    },

    less: {
        files: [ '<%= app_files.less %>' ],
        tasks: [ 'less:build' ]
    },

    tpls: {
        files: [ '<%= app_files.tpl %>' ],
        tasks: [ 'copy:build_app_tpl' ]
    },

    amd: {
        files: [ 'templates/config-amd.tpl.js' ],
        task: [ 'amd-config:build' ]
    },

    test: {
        files: [
            '<%= app_files.js %>',
            '<%= app_files.spec %>',
        ]
    }
};
