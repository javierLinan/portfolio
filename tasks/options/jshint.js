module.exports = {
    src: [ 
        '<%= app_files.js %>'
    ],

    gruntfile: [
        'Gruntfile.js'
    ],
  
    options: {
        curly: true,
        immed: true,
        newcap: true,
        noarg: true,
        sub: true,
        boss: true,
        eqnull: true,
        debug: true
    },
    
    globals: {}
};