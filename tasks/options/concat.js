module.exports = {
    build_css: {
        src: [
        '<%= vendor_files.css %>',
        '<%= dir.build %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
        ],
        dest: '<%= dir.build %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
    },
    compile_css: {
        src: [
        '<%= vendor_files.css %>',
        '<%= dir.compile %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
        ],
        dest: '<%= dir.compile %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
    }
};
