module.exports = {
    jsdoc: {
        src: ['<%= app_files.js %>'],
        options: {
            destination: '<%= dir.doc %>'
        }
    }
};
