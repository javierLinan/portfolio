module.exports = {
    build: {
        files: {
            '<%= dir.build %>/assets/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'
        }
    },
    compile: {
        files: {
            '<%= dir.compile %>/assets/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'
        },
        options: {
            cleancss: true,
            compress: true
        }
    }
};
