module.exports = {
    build: {
        src: [
            '<%= dir.build %>',
        ]
    },
    compile: {
        src: [
            '<%= dir.compile %>'
        ]
    }
};
