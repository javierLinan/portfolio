module.exports = {
    build_app_assets: {
        files: [
        {
            src: [ '**' ],
            dest: '<%= dir.build %>/assets/',
            cwd: '<%= dir.src %>/assets',
            expand: true
        }
       ]
    },
    build_vendor_assets: {
        files: [
        {
          src: [ '<%= vendor_files.assets %>' ],
          dest: '<%= dir.build %>/assets/',
          cwd: '.',
          expand: true,
          flatten: true
        }
        ]
     },
    build_app_js: {
        files: [
        {
          src: [ '<%= app_files.js %>' ],
          dest: '<%= dir.build %>/',
          cwd: '.',
          expand: true
        }
        ]
    },
    build_vendor_js: {
        files: [
        {
            src: [ '<%= vendor_files.js %>' ],
            dest: '<%= dir.build %>/',
            cwd: '.',
            expand: true
        }
        ]
    },
    build_app_tpl: {
        files: [
        {
            src: [ '<%= app_files.tpl %>' ],
            dest: '<%= dir.build %>/',
            cwd: '.',
            expand: true
        }
        ]
    },
    compile_app_assets: {
        files: [
            {
                src: [ '**' ],
                dest: '<%= dir.compile %>/assets',
                cwd: '<%= dir.src %>/assets',
                expand: true
            }
        ]
    }
};
