module.exports = {
    build: {
        tpl: 'index-build.tpl.html',
        dir: '<%= dir.build %>',
    },
    compile: {
        tpl: 'index-compile.tpl.html',
        dir: '<%= dir.compile %>',
    },
};
