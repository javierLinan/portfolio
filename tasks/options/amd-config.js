module.exports = {
    build: {
        dir: '<%= dir.build %>',
        tpl: 'config-amd.tpl.js',
        entry_point: '<%= app_files.entry_point %>',
    },
    test: {
        dir: '<%= dir.test %>',
        tpl: 'config-amd-test.tpl.js',
        src: '<%= app_files.spec %>',
    }
};
