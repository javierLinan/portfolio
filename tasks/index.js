module.exports = function(grunt) {

    grunt.registerMultiTask( 'index', 'Process index.html template', function () {
        var these = this;

        grunt.file.copy(grunt.config('dir.templates') + '/' + this.data.tpl, this.data.dir + '/index.html', {
            process: function ( contents, path ) {
                return grunt.template.process( contents, {
                    data: Object.assign(these.data, {
                        pkg: grunt.config('pkg')
                    })
                });
            }
        });
    });
};
