# PORTFOLIO #

This is the code of my portfolio, which was created using Backbone.js + Require.js. I'm not using Marionette.js or React.js for helping with Backbone Views. The purpose is to achieve the creation of my own solution to some of the lacks in Backbone without using any of these useful libraries.

Some examples will be added in order to practice my Javascript/Backbone skills.

## How do I get set up? ##

Install Node.js and then:

```
#!javascript

$ git clone https://javierLinan@bitbucket.org/javierLinan/portfolio.git
$ cd portfolio
$ sudo npm -g install grunt-cli bower
$ npm install
$ bower install
$ grunt watch
$ An accessible index.html will be generated in "bin" folder
```

## How do I generate javascript documentation? ##

```
#!javascript

$ cd portfolio
$ grunt jsdoc
$ The documentation will be created in the "doc" folder
```