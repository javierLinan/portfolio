define([
    'moduleExamples/dictionary/view-word',
    'moduleHelpers/closeableview',
], function (WordView, CloseableView) {
    'use strict';

    describe("WordView", function() {
        it("should be instance of CloseableView", function() {
            var wv = new WordView();
            wv.should.be.instanceof(CloseableView);
        });
    });
});
