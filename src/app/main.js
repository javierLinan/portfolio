/*global require*/

require([
    'backbone',
    'moduleMain/router-main',
    'moduleHelpers/handlebars',    
    'bootstrap'
], function (Backbone, MainRouter) {
    'use strict';

    /**
     * @name module:Main.Main/Main
     * @author José Javier Liñán Manzaneda
     * @requires Backbone
     * @requires Main/MainRouter
     */

    new MainRouter();

    Backbone.history.start(); 
});