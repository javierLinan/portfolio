/*global define*/

/**
 * @exports module:Main.Main/MainRouter
 */

define([
    'backbone',
    'moduleMain/view-main',
    'moduleCommon/globals'
], function (Backbone, MainView, GLOBALS) {
    'use strict';

    var MainRouter = Backbone.Router.extend(/** @lends MainRouter.prototype */{

        /** 
         * @name module:Main.Main/MainRouter
         * @class MainRouter
         * @author José Javier Liñán Manzaneda
         * @augments Backbone.Router
         * @requires Backbone
         * @requires Main/MainView
         * @requires Common/GLOBALS
         * @contructs HomeRouter object 
         * */        

        initialize: function () {

            this.cached = {
                mainView: new MainView ()
            };
        },

        routes: {
            ''                   : 'homeCtrl',
            '!/examples'         : 'examplesCtrl',
            '!/examples/:example': 'exampleCtrl'
        },

        /**
         * Controller for the Home page
         */

        homeCtrl: function () {

            this.cached.mainView.navigateSubview (['home']); 
        }, 

        /**
         * Controller for the Examples page. It's redirected to the first example page.
         */

        examplesCtrl: function () {

            this.navigate('!/examples/dictionary');
            this.cached.mainView.navigateSubview (['examples', 'dictionary']);
        },  

        /**
         * Controller for the different examples depending of the example parameter
         * 
         * @param {string} example The ID of the example to be rendered
         */

        exampleCtrl: function (example) {

            this.cached.mainView.navigateSubview (['examples', example]); 
        }
    });

    return MainRouter;
});
