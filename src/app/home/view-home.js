/*global define*/

/**
 * @exports module:Home.Home/HomeView
 */

define([
    'handlebars',
    'text!moduleHome/home.tpl.html',
    'moduleHelpers/closeableview',
    'moduleCommon/globals'    
], function (Handlebars, homeTemplate, CloseableView, GLOBALS) {
    'use strict';

    var HomeView = CloseableView.extend(/** @lends HomeView.prototype */{

        /** 
         * @name module:Home.Home/HomeView
         * @class HomeView
         * @author José Javier Liñán Manzaneda
         * @augments CloseableView
         * @requires Handlebars
         * @requires homeTemplate
         * @requires CloseableView
         * @requires GLOBALS
         * @contructs HomeView object
         *
         * @param {Map} param
         * @param {Object} param.langswitcher Reference to the lang object
         * @param {Object} param.lang Reference to the language switcher
         */
        
        initialize: function (param) {

            this.cached = {
                
                //An reference to the object which manage the language files
                lang: param.lang,

                //An reference to the object which manage the current language and the language switcher
                langswitcher: param.langswitcher,

                template: Handlebars.default.compile(homeTemplate)
            }; 
        },

        render: function () {

            //We obtain the current lang, which is used in conjunction with the path to
            //the translations to obtain then.
            var lang = this.cached.langswitcher.getCurrentLang(),
                dataLang = this.cached.lang.getDataLang(GLOBALS.LANG_PATH_HOME, lang),
                tmplRendered = this.cached.template(dataLang);    

            //The document title is updated        
            document.title = dataLang.pageTitle;

            this.$el.html(tmplRendered);

            return this;            
        }      
    });


    return HomeView;
});
