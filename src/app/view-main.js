/*global define*/

/**
 * @exports module:Main.Main/MainView
 */

define([
    'handlebars',
    'canvasloader',
    'moduleHome/view-home',
    'moduleExamples/view-examples',
    'text!moduleMain/main.tpl.html',
    'moduleHelpers/closeableview',    
    'moduleHelpers/langswitcher',
    'moduleHelpers/lang',
    'moduleCommon/globals'
], function (Handlebars, CanvasLoader, HomeView, ExamplesView, mainTemplate, CloseableView, Langswitcher, Lang, GLOBALS) {
    'use strict';

    var MainView = CloseableView.extend(/** @lends MainView.prototype */{

        /** 
         * @name module:Main.Main/MainView
         * @class MainView
         * @author José Javier Liñán Manzaneda
         * @augments CloseableView
         * @requires Handlebars
         * @requires CanvasLoader
         * @requires Home/HomeView
         * @requires Examples/ExamplesView
         * @requires Main/mainTemplate
         * @requires Helpers/CloseableView
         * @requires Helpers/Langswitcher
         * @requires Helpers/Lang
         * @requires Common/GLOBALS
         * @contructs MainView object 
         * */

        initialize: function () {

            this.cached = {

                //The DOM id of the element where the sub-views will be rendered
                containerId: '#' + GLOBALS.DOM_CONTENT_MAIN_ID,

                //An instance of the object which manage the language files
                lang: new Lang (),

                //An instance of the object which manage the current language and the language switcher
                langswitcher: new Langswitcher ({
                    defaultLang : GLOBALS.LANG_DEFAULT,
                    langsAllowed: GLOBALS.LANG_ALLOWED
                }),

                template: Handlebars.default.compile(mainTemplate),

                subviews: []
            };

            //The main view is rendered herself   
            this.render ();
        },

        el: '#' + GLOBALS.DOM_APP_ID,

        events: {
            'click ul.switcher li ': 'changeLanguage'
        },        

        render: function () {

            //We obtain the current lang, which is used in conjunction with the path to
            //the translations to obtain then.

            var lang = this.cached.langswitcher.getCurrentLang(),
                dataLang = this.cached.lang.getDataLang(GLOBALS.LANG_PATH_MAIN, lang),
                tmplRendered = this.cached.template(_.extend(dataLang, {'lang': lang}));

            // Main view is rendered and then the loader is shown in the sub-views DOM container
            this.$el.html(tmplRendered);

            // this.showloader();
        },

        /**
         * Method called when the language switcher is clicked
         *
         * @param {object} event The event received after the DOM action
         */

        changeLanguage: function (event) {

            //If the switcher changes his state, the main view and its subviews must be reloaded
            if (this.cached.langswitcher.updateLangSwitcher (event)) {
                this.renderNested ();
            }
        },

        // showloader: function () {

        //     //TODO: The loader configuration should be stored in a config file (maybe globals.js)

        //     if (!this.cached.loader) {
        //         this.cached.loader = new CanvasLoader(GLOBALS.DOM_CONTENT_MAIN_ID);
        //         this.cached.loader.setShape('spiral'); 
        //         this.cached.loader.setDiameter(69); 
        //         this.cached.loader.setDensity(9);
        //         this.cached.loader.setRange(1.4); 
        //         this.cached.loader.setSpeed(1);
        //         this.cached.loader.setFPS(10);
        //     } 

        //     this.cached.loader.show();
        // },

        /**
         * Method used for the purpose of automating instantiation in the sub-views
         *
         * @param {string} viewName ID name for the sub-view
         * @return {object} Instance of a sub-view
         */

        instantiateSubview: function (viewName) {

            //Each sub-view receives a reference to the lang object and the language 
            //switcher object in order to be able of accessing to the translations and 
            //the current language

            var instance,
                param = {
                    langswitcher: this.cached.langswitcher,
                    lang: this.cached.lang
                };

            switch (viewName) {
                case 'home': 
                    instance = this.cached.subviews['home'] = new HomeView (param);
                    break;
                case 'examples':
                    instance = this.cached.subviews['examples'] = new ExamplesView (param);
                    break;
            }

            return instance;
        }
    });

    return MainView;
});
