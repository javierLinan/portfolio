/*global define*/

define([
    'jquery',
    'handlebars',
    'text!moduleExamples/hiddenletters/hiddenletters.tpl.html',
    'moduleHelpers/closeableview',
    'moduleCommon/globals'
], function ($, Handlebars, hiddenlettersTemplate, CloseableViewHelper, GLOBALS) {
    'use strict';

    var HiddenlettersView = CloseableViewHelper.extend(/** @lends HiddenlettersView.prototype */{

        /** @class HiddenlettersView
        * @author José Javier Liñán Manzaneda
        * @augments CloseableViewHelper
        * @contructs HiddenlettersView object */          

        template: Handlebars.default.compile(hiddenlettersTemplate),

        initialize: function (param) {    

            this._langswitcher = param.langswitcher;
            this._lang = param.lang;
        },

        render: function () {

            var lang = this._langswitcher.getCurrentLang (),
                dataLang = this._lang.getDataLang(GLOBALS.LANG_PATH_DICTIONARY, lang);

            document.title = dataLang.pageTitle;

            this.$el.html(this.template(dataLang));               

            this.setNavOption ();

            return this;                         
        },

        setNavOption: function () {

            $('#' + GLOBALS.DOM_CONTENT_EXAMPLES_ID + ' .nav')
                .find('li.active')
                .removeClass('active')
                .end()
                .find('li.tab-hiddenletters')
                .addClass('active');
        }
    });

    return HiddenlettersView;
});
