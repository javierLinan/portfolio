/*global define*/

/**
 * @exports module:Examples.Examples.Dictionary/WordView
 */

define([
    'jquery',
    'handlebars',
    'text!moduleExamples/dictionary/word.tpl.html',
    'moduleHelpers/closeableview',
    'moduleCommon/globals'
], function ($, Handlebars, wordTemplate, CloseableView, GLOBALS) {
    'use strict';

    var WordView = CloseableView.extend(/** @lends WordView.prototype */{

        /**
         * @name module:Examples.Examples.Dictionary/WordView
         * @class WordView
         * @author José Javier Liñán Manzaneda
         * @augments CloseableView
         * @requires Jquery
         * @requires Handlebars
         * @requires Examples.Dictionary/wordTemplate
         * @requires Helpers/CloseableView
         * @requires Common/GLOBALS
         * @contructs WordView object
         *
         * @param {Map} param
         * @param {Object} param.langswitcher Reference to the lang object
         * @param {Object} param.lang Reference to the language switcher
         **/

        initialize: function (param) {

            if(!param) {
                return;
            }

            this.cached = {

                //An reference to the object which manage the language files
                lang: param.lang,

                //An reference to the object which manage the current language and the language switcher
                langswitcher: param.langswitcher,

                template: Handlebars.default.compile(wordTemplate)
            };

            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
        },

        tagName: 'tr',

        events: {
            'click .destroy': 'clear',
            'click .change' : 'edit',
            'click .update' : 'update'
        },

        render: function () {

            //We obtain the current lang, which is used in conjunction with the path to
            //the translations to obtain then.
            var lang = this.cached.langswitcher.getCurrentLang(),
                dataLang = this.cached.lang.getDataLang(GLOBALS.LANG_PATH_WORD, lang),
                tmplRendered = this.cached.template(_.extend(this.model.toJSON(), dataLang));

            this.$el.html(tmplRendered);

            return this;
        },

        clear: function () {
            this.model.destroy();
            this.trigger('remove', this);
        },

        edit: function () {
            this.$el.addClass('editing');
            this.$('input:eq(0)').focus();
        },

        update: function () {
            var valueTerm = this.$('input:eq(0)').val(),
                valueTrans = this.$('input:eq(1)').val(),
                trimmedValueTerm = valueTerm.trim(),
                trimmedValueTrans = valueTrans.trim();

            if (trimmedValueTerm && trimmedValueTrans) {
                this.model.save({
                    french: trimmedValueTerm,
                    spanish: trimmedValueTrans
                });

                if (valueTerm !== trimmedValueTerm || valueTrans !== trimmedValueTrans) {
                    this.model.trigger('change');
                }
            } else {
                this.clear();
            }

            this.$el.removeClass('editing');
        }
    });

    return WordView;
});
