/*global define*/

/**
 * @exports module:Examples.Examples.Dictionary/DictionaryView
 */

define([
    'jquery',
    'handlebars',
    'moduleExamples/dictionary/collection-words',
    'moduleExamples/dictionary/view-word',
    'text!moduleExamples/dictionary/dictionary.tpl.html',
    'moduleHelpers/closeableview',
    'moduleCommon/globals'
], function ($, Handlebars, WordsCollection, WordView, dictionaryTemplate, CloseableView, GLOBALS) {
    'use strict';

    var DictionaryView = CloseableView.extend(/** @lends DictionaryView.prototype */{

        /** 
         * @name module:Examples.Examples.Dictionary/DictionaryView
         * @class DictionaryView
         * @author José Javier Liñán Manzaneda
         * @augments CloseableView
         * @requires Jquery
         * @requires Handlebars
         * @requires Examples.Dictionary/WordsCollection
         * @requires Examples.Dictionary/WordView
         * @requires Examples.Dictionary/dictionaryTemplate
         * @requires Helpers/CloseableView
         * @requires Common/GLOBALS
         * @contructs ExamplesView object 
         *
         * @param {Map} param
         * @param {Object} param.langswitcher Reference to the lang object
         * @param {Object} param.lang Reference to the language switcher
         **/      

        initialize: function (param) {        

            this.cached = {

                //The DOM id of the element where the sub-views will be rendered
                containerId: '#' + GLOBALS.DOM_CONTENT_DICTIONARY_ID,
                
                //An reference to the object which manage the language files
                lang: param.lang,

                //An reference to the object which manage the current language and the language switcher
                langswitcher: param.langswitcher,

                template: Handlebars.default.compile(dictionaryTemplate)
            };            

            this.listenTo(WordsCollection, 'add', this.addWord);
            this.listenTo(WordsCollection, 'reset', this.addAllWords);
        },

        events: {
            'click .add': 'createOnClick'
        },

        render: function () {

            //We obtain the current lang, which is used in conjunction with the path to
            //the translations to obtain then.
            var lang = this.cached.langswitcher.getCurrentLang(),
                dataLang = this.cached.lang.getDataLang(GLOBALS.LANG_PATH_DICTIONARY, lang),
                tmplRendered = this.cached.template(dataLang);                   

            //The document title is updated        
            document.title = dataLang.pageTitle;

            this.$el.html(tmplRendered);           

            this.setNavOption ();

            this.$termInput = this.$('#entry-term');
            this.$translationInput = this.$('#entry-translation');
            this.$wordsList = this.$('#words');
            this.$addButton = this.$('.add');

            if (this.renderedSubviewsrenderedSubviews) {
                for (var i = 0; i < this.renderedSubviewsrenderedSubviews.length; i++) {
                    this.$('#words tbody').append(this.renderedSubviewsrenderedSubviews[i].render().el);
                    this.renderedSubviewsrenderedSubviews[i].delegateEvents();
                }
            } else {
                WordsCollection.fetch({reset:true});
            }

            return this;                         
        },

        renderNestedSubviews: function () {

            for (var i = 0; i < this.renderedSubviewsrenderedSubviews.length; i++) {
                this.renderedSubviewsrenderedSubviews[i].$el.empty();
                this.renderedSubviewsrenderedSubviews[i].undelegateEvents();                 
                this.$('#words tbody').append(this.renderedSubviewsrenderedSubviews[i].render().el);
                this.renderedSubviewsrenderedSubviews[i].delegateEvents();
            }
        },

        addWord: function (word) {

            var view = new WordView({ 
                model       : word,
                langswitcher: this.cached.langswitcher,
                lang        : this.cached.lang                
            });

            this.addToRendered(view);

            view.on('remove', this.removeFromRendered, this);

            this.$('#words tbody').append(view.render().el);
        },

        addAllWords: function () {
            // this.$wordsList.empty();
            WordsCollection.each(this.addWord, this);
        },        

        newAttributes: function () {
            return {
                french : this.$termInput.val().trim(),
                spanish: this.$translationInput.val().trim()
            };
        },        

        createOnClick: function (e) {

            WordsCollection.create(this.newAttributes());
            this.$termInput.val('');
            this.$translationInput.val('');
        },        

        setNavOption: function () {

            $('#' + GLOBALS.DOM_CONTENT_EXAMPLES_ID + ' .nav')
                .find('li.active')
                .removeClass('active')
                .end()
                .find('li.tab-dictionary')
                .addClass('active');
        }
    });

    return DictionaryView;
});
