/*global define*/

/**
 * @exports module:Examples.Examples.Dictionary/WordModel
 */


define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var WordModel = Backbone.Model.extend({
       
        defaults: {
            french: '',
            spanish: ''
        }
    });

    return WordModel;
});
