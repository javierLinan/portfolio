/*global define */

/**
 * @exports module:Examples.Examples.Dictionary/WordCollection
 */


define([
    'underscore',
    'backbone',
    'backboneLocalstorage',
    'moduleExamples/dictionary/model-word'
], function (_, Backbone, Store, WordModel) {
    'use strict';

    var WordsCollection = Backbone.Collection.extend({

        model: WordModel,

        localStorage: new Store('dictionary')
    });

    return new WordsCollection();
});
