/*global define*/

/**
 * @exports module:Examples.Examples/ExamplesView
 */

define([
    'handlebars',
    'moduleExamples/dictionary/view-dictionary',
    'moduleExamples/hiddenletters/view-hiddenletters',
    'text!moduleExamples/examples.tpl.html',
    'moduleHelpers/closeableview',
    'moduleCommon/globals'    
], function (Handlebars, DictionaryView, HiddenlettersView, examplesTemplate, CloseableView, GLOBALS) {
    'use strict';

    var ExamplesView = CloseableView.extend(/** @lends ExamplesView.prototype */{

        /** 
         * @name module:Examples.Examples/ExamplesView
         * @class ExamplesView
         * @author José Javier Liñán Manzaneda
         * @augments CloseableView
         * @requires Handlebars
         * @requires Examples.Dictionary/DictionaryView
         * @requires Examples.HiddenLetters/HiddenLettersView
         * @requires Examples/examplesTemplate
         * @requires Helpers/CloseableView
         * @requires Common/GLOBALS
         * @contructs ExamplesView object 
         *
         * @param {Map} param
         * @param {Object} param.langswitcher Reference to the lang object
         * @param {Object} param.lang Reference to the language switcher
         **/

        initialize: function (param) {        

            this.cached = {

                //The DOM id of the element where the sub-views will be rendered
                containerId: '#' + GLOBALS.DOM_CONTENT_EXAMPLE_ID,
                
                //An reference to the object which manage the language files
                lang: param.lang,

                //An reference to the object which manage the current language and the language switcher
                langswitcher: param.langswitcher,

                template: Handlebars.default.compile(examplesTemplate),

                subviews: []
            };             
        },

        render: function () {

            //We obtain the current lang, which is used in conjunction with the path to
            //the translations to obtain then.
            var lang = this.cached.langswitcher.getCurrentLang(),
                dataLang = this.cached.lang.getDataLang(GLOBALS.LANG_PATH_EXAMPLES, lang),
                tmplRendered = this.cached.template(dataLang);    

            //The document title is updated        
            document.title = dataLang.pageTitle;

            this.$el.html(tmplRendered);

            return this;            

        },

        instantiateSubview: function (viewName) {

            //Each sub-view receives a reference to the lang object and the language 
            //switcher object in order to be able of accessing to the translations and 
            //the current language
            
            var instance = null,
                param = {
                    langswitcher: this.cached.langswitcher,
                    lang: this.cached.lang
                };

            switch (viewName) {
                case 'dictionary': 
                    instance = this.cached.subviews['dictionary'] = new DictionaryView (param);
                    break;

                case 'hiddenletters':
                    instance = this.cached.subviews['hiddenletters'] = new HiddenlettersView (param);
                    break;
            }

            return instance;
        }
    });

    return ExamplesView;
});
