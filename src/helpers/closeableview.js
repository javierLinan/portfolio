/*global define*/

/**
 * @exports module:Helpers.Helpers/CloseableView
 */

define([
    'backbone',
], function (Backbone) {

    var CloseableView = Backbone.View.extend(/** @lends WordView.prototype */{

        /**
         * @name module:Helpers.Helpers/CloseableView
         * @class CloseableView
         * @author José Javier Liñán Manzaneda
         * @augments Backbone.View
         * @requires Backbone
         * @contructs CloseableView object
         *
         * This method adds (if not already) a view to the array of sub-views rendered currently
         *
         * @param {object} subView Reference to a view
         * @return {object} The view introduced
         * */

        addToRendered: function (subView) {

            if (!this.renderedSubviews) {
                this.renderedSubviews = [];
            }

            if (_.indexOf(this.renderedSubviews, subView) === -1) {
                this.renderedSubviews.push(subView);
            }

            return subView;
        },

        /**
         * This method removes the su-view as argument from the array of sub-views rendered
         *
         * @param {Object} subView The sub-view to be removed
         */

        removeFromRendered: function(subView) {

            this.renderedSubviews = _.without(this.renderedSubviews, subView);
        },

        /**
         * It closes the subview who is calling the method and all his rendered sub-views
         */

        closeSubview: function() {

            this.$el.empty();
            this.undelegateEvents();

            if (this.renderedSubviews) {
                for (var i = 0; i < this.renderedSubviews.length; i++) {
                    this.renderedSubviews[i].closeSubview();
                }
            }

            if (this.onClose) {
                this.onClose();
            }
        },

        /**
         * The method closes all the sub-views in the subviews rendered array and he removes them from it
         */

        emptyRendered: function () {

            if (this.renderedSubviews) {
                for (var i = 0; i < this.renderedSubviews.length; i++) {
                    this.renderedSubviews[i].closeSubview();
                    this.removeFromRendered(this.renderedSubviews[i]);
                }
            }
        },

        /**
         * The metod retrieves the sub-view which name is passed as argument from the sub-views array.
         * If this doesn't exists, is created and added to the array
         *
         * @param {String} subviewName The ID name of the sub-view
         * @return {Object} The sub-view
         */

        registerSubview: function (subviewName) {

            var subview;

            if (!this.subviews) {
                this.subviews = [];
            }

            if (!this.subviews[subviewName]) {
                subview = this.instantiateSubview (subviewName);
            } else {
                subview = this.subviews[subviewName];
            }

            return subview;
        },

        /**
         * The method is used with the purpose of render nested subviews in an efficient way
         *
         * @param {Array} path Array of strings with the IDs of the nested views in the order which
         * they should be rendered. Example: ['examples', 'dictionary']
         */

        navigateSubview: function (path) {

            var subviewName = path[0],
                subpath = path.slice(1),
                subview = this.registerSubview(subviewName);

            if (subview) {
                if (!this.renderedSubviews) {
                    this.renderedSubviews = [];
                }

                if (_.indexOf(this.renderedSubviews, subview) === -1) {
                    this.emptyRendered ();
                    this.addToRendered (subview);
                }

                subview.setElement($(this.cached.containerId, this.$el));
                subview.render ();

                if (subpath.length > 0) {
                    subview.navigateSubview (subpath);
                }
            }
        },

        /**
         * This method reload the view who is calling him and all the sub-views rendered in that moment
         */

        renderNested: function () {

            this.render ();

            if (this.renderedSubviews) {

                //If the subview has a custom render method, it's called

                if (this.renderNestedSubviews) {
                    this.renderNestedSubviews ();
                } else {
                    for (var i = 0; i < this.renderedSubviews.length; i++) {
                        this.renderedSubviews[i].$el.empty();
                        this.renderedSubviews[i].undelegateEvents();
                        this.renderedSubviews[i].setElement($(this.cached.containerId, this.$el));
                        this.renderedSubviews[i].renderNested ();
                    }
                }
            }
        }
    });

    return CloseableView;
});
