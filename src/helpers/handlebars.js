/*global define*/

define([
    'handlebars',
], function (Handlebars) {

    Handlebars.default.registerHelper ('if_eq', function (a, b, opts) {
        if(a === b) {
            return opts.fn(this);
        } else {
            return opts.inverse(this);
        }
    });

});