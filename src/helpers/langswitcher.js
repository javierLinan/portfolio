/*global define, navigator*/

define([], function () {
    
    function Langswitcher (param) {
        this.lang = param.defaultLang;
        this.langsAllowed = param.langsAllowed;

        this.initCurrentLang ();
    }

    Langswitcher.prototype.initCurrentLang = function () {
        var lang = navigator.language || navigator.userLanguage,
            codeLang = lang.substr(0,2).toLowerCase();

        this.setCurrentLang (codeLang);
    };

    Langswitcher.prototype.setCurrentLang = function (lang) {
        if (this.langsAllowed.indexOf(lang) > -1) {
            this.lang = lang;
        }
    };

    Langswitcher.prototype.getCurrentLang = function () {
        return this.lang;
    };

    Langswitcher.prototype.updateLangSwitcher = function (event) {

        var lang;

        if (event) {
            lang = event.currentTarget.classList[0];

            if(lang !== this.getCurrentLang() ) {
                this.setCurrentLang (lang);
                return true;
            }
        }

        return false;
    };

    return Langswitcher; 
});