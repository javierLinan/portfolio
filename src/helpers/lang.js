/*global define*/

define([
    'text!moduleLang/es.json',    
    'text!moduleLang/fr.json',    
], function (esLang, frLang) {
    
    function Lang () {
        this.langs = {
            'es': JSON.parse(esLang),
            'fr': JSON.parse(frLang)
        };

    }

    Lang.prototype.getDataLang = function (module, codeLang) {
        var path = module.split('.');

        function getSubObject (obj, path) {
            var currentId = path[0],
                subObj = obj[currentId],
                remainingPath = path.slice(1);

            if (remainingPath.length > 0) {
                return getSubObject (subObj, remainingPath);
            } else {
                return subObj;
            }
        }

        return getSubObject (this.langs[codeLang], path);        
    };

    return Lang; 
});