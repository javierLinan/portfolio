/*global define*/

define([], function () {
    'use strict';

    return {
        DOM_APP_ID               : 'app',
        DOM_CONTENT_MAIN_ID      : 'content-inner',
        DOM_CONTENT_EXAMPLES_ID  : 'examples',
        DOM_CONTENT_EXAMPLE_ID   : 'example',
        DOM_CONTENT_DICTIONARY_ID: 'words',
        
        LANG_DEFAULT        : 'es',
        LANG_ALLOWED        : ['es', 'fr'],
        LANG_PATH_MAIN      : 'main',
        LANG_PATH_HOME      : 'home',
        LANG_PATH_EXAMPLES  : 'examples',
        LANG_PATH_DICTIONARY: 'examples.dictionary',
        LANG_PATH_WORD      : 'examples.dictionary.word',
        
        ENTER_KEY     : 13,
        ESCAPE_KEY    : 27
    };
});
