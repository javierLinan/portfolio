require.config({
    baseUrl: '',
    paths: {
        'mocha'               : '../node_modules/mocha/mocha',
        'chai'                : '../node_modules/chai/chai',
        'jquery'              : '../vendor/jquery/dist/jquery',
        'underscore'          : '../vendor/lodash/dist/lodash.underscore',
        'backbone'            : '../vendor/backbone/backbone',
        'text'                : '../vendor/requirejs-text/text',
        'backboneLocalstorage': '../vendor/backbone.localstorage/backbone.localStorage',
        'handlebars'          : '../vendor/handlebars/handlebars.amd',
        'bootstrap'           : '../vendor/bootstrap/dist/js/bootstrap',
        'canvasloader'        : '../vendor/canvasloader/js/heartcode-canvasloader',
        'moduleMain'          : '../src/app',
        'moduleHelpers'       : '../src/helpers',
        'moduleCommon'        : '../src/common',
        'moduleHome'          : '../src/app/home',
        'moduleExamples'      : '../src/app/examples',
        'moduleLang'          : '../assets/lang',
    },
    shim: {
        mocha: {
            exports: 'mocha'
        },
        chai: {
            exports: 'chai'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        backboneLocalstorage: {
            deps   : ['backbone'],
            exports: 'Store'
        },
    },
    urlArgs: 'bust=' + (new Date()).getTime(),
});

define(function(require) {
    var mocha = require('mocha');
    var chai = require('chai');
    var should = chai.should();

    mocha.setup('bdd');
    mocha.bail(false);

    require([
        <% src.forEach(function(file) { %>'<%= file %>',<% }); %>
    ], function() {
        mocha.run();
    });
});
