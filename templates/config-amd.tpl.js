require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        backboneLocalstorage: {
            deps   : ['backbone'],
            exports: 'Store'
        },
        bootstrap: {
            deps   : ['jquery'],
            exports: 'Bootstrap'
        },
        canvasloader: {
            exports: 'CanvasLoader'
        }
    },



    paths: {
        jquery              : 'vendor/jquery/dist/jquery',
        underscore          : 'vendor/lodash/dist/lodash.underscore',
        backbone            : 'vendor/backbone/backbone',
        text                : 'vendor/requirejs-text/text',
        backboneLocalstorage: 'vendor/backbone.localstorage/backbone.localStorage',
        handlebars          : 'vendor/handlebars/handlebars.amd',
        bootstrap           : 'vendor/bootstrap/dist/js/bootstrap',
        canvasloader        : 'vendor/canvasloader/js/heartcode-canvasloader',
        moduleMain          : 'src/app',
        moduleHelpers       : 'src/helpers',
        moduleCommon        : 'src/common',
        moduleHome          : 'src/app/home',
        moduleExamples      : 'src/app/examples',
        moduleLang          : 'assets/lang'
    },
    deps: ['<%= entry_point %>']
});
